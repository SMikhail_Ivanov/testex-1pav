﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestEx
{
    class FunctionForSearch
    {
        public void CheckFileCount(string CurrentString, int FileCount, Label LabelForFileCount, Label LabelForFileName)
        {
            LabelForFileCount.Text = FileCount.ToString();
            CheckFileName(CurrentString, LabelForFileName);
        }
        public void CheckFileName(string CurrentString, Label LabelForFileName)
        {
            string NewString = CurrentString;
            int CountNewString = NewString.Length - 1;
            string NewName = "";
            var Care = '\\';

            while (NewString.ElementAt(CountNewString) != Care)
            {
                NewName = NewString.ElementAt(CountNewString) + NewName;
                CountNewString--;
            }
            LabelForFileName.Text = NewName;
        }
    }
}
