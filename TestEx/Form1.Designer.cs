﻿namespace TestEx
{
    partial class FormForTestEx
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.TreeViewForEx1 = new System.Windows.Forms.TreeView();
            this.TreeViewForInfo = new System.Windows.Forms.TreeView();
            this.OpenFileDialogForEx1 = new System.Windows.Forms.OpenFileDialog();
            this.SetDirWayButton = new System.Windows.Forms.Button();
            this.FolderBrowserDialogForEx1 = new System.Windows.Forms.FolderBrowserDialog();
            this.SetFileButton = new System.Windows.Forms.Button();
            this.FindTextTextBox = new System.Windows.Forms.TextBox();
            this.FindNameTextBox = new System.Windows.Forms.TextBox();
            this.NewTreeViewForEx1 = new System.Windows.Forms.TreeView();
            this.StopSearchButton = new System.Windows.Forms.Button();
            this.ResumeSearchButton = new System.Windows.Forms.Button();
            this.CrashSearchButton = new System.Windows.Forms.Button();
            this.ClearSearchButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.FileCountLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ThreadFileNameLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SearchedListBox = new System.Windows.Forms.ListBox();
            this.SearchedRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // TreeViewForEx1
            // 
            this.TreeViewForEx1.Location = new System.Drawing.Point(9, 9);
            this.TreeViewForEx1.Margin = new System.Windows.Forms.Padding(0);
            this.TreeViewForEx1.Name = "TreeViewForEx1";
            this.TreeViewForEx1.Size = new System.Drawing.Size(302, 575);
            this.TreeViewForEx1.TabIndex = 0;
            // 
            // TreeViewForInfo
            // 
            this.TreeViewForInfo.Location = new System.Drawing.Point(1051, 12);
            this.TreeViewForInfo.Name = "TreeViewForInfo";
            this.TreeViewForInfo.Size = new System.Drawing.Size(404, 162);
            this.TreeViewForInfo.TabIndex = 2;
            // 
            // SetDirWayButton
            // 
            this.SetDirWayButton.Location = new System.Drawing.Point(345, 12);
            this.SetDirWayButton.Name = "SetDirWayButton";
            this.SetDirWayButton.Size = new System.Drawing.Size(180, 23);
            this.SetDirWayButton.TabIndex = 4;
            this.SetDirWayButton.Text = "Выбор директории";
            this.SetDirWayButton.UseVisualStyleBackColor = true;
            this.SetDirWayButton.Click += new System.EventHandler(this.SetDirWayButton_Click);
            // 
            // SetFileButton
            // 
            this.SetFileButton.Location = new System.Drawing.Point(900, 12);
            this.SetFileButton.Name = "SetFileButton";
            this.SetFileButton.Size = new System.Drawing.Size(106, 23);
            this.SetFileButton.TabIndex = 5;
            this.SetFileButton.Text = "Выбор файла";
            this.SetFileButton.UseVisualStyleBackColor = true;
            this.SetFileButton.Click += new System.EventHandler(this.SetFileButton_Click);
            // 
            // FindTextTextBox
            // 
            this.FindTextTextBox.Location = new System.Drawing.Point(345, 41);
            this.FindTextTextBox.Name = "FindTextTextBox";
            this.FindTextTextBox.Size = new System.Drawing.Size(180, 20);
            this.FindTextTextBox.TabIndex = 6;
            this.FindTextTextBox.Text = "Файл содержит текст";
            this.FindTextTextBox.Click += new System.EventHandler(this.FindTextTextBox_Click);
            // 
            // FindNameTextBox
            // 
            this.FindNameTextBox.Location = new System.Drawing.Point(345, 67);
            this.FindNameTextBox.Name = "FindNameTextBox";
            this.FindNameTextBox.Size = new System.Drawing.Size(180, 20);
            this.FindNameTextBox.TabIndex = 7;
            this.FindNameTextBox.Text = "Файл имеет имя";
            this.FindNameTextBox.Click += new System.EventHandler(this.FindNameTextBox_Click);
            // 
            // NewTreeViewForEx1
            // 
            this.NewTreeViewForEx1.Location = new System.Drawing.Point(556, 12);
            this.NewTreeViewForEx1.Margin = new System.Windows.Forms.Padding(0);
            this.NewTreeViewForEx1.Name = "NewTreeViewForEx1";
            this.NewTreeViewForEx1.Size = new System.Drawing.Size(302, 575);
            this.NewTreeViewForEx1.TabIndex = 8;
            this.NewTreeViewForEx1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.NewTreeViewForEx1_AfterSelect);
            // 
            // StopSearchButton
            // 
            this.StopSearchButton.Location = new System.Drawing.Point(345, 93);
            this.StopSearchButton.Name = "StopSearchButton";
            this.StopSearchButton.Size = new System.Drawing.Size(180, 23);
            this.StopSearchButton.TabIndex = 9;
            this.StopSearchButton.Text = "Остановить поиск";
            this.StopSearchButton.UseVisualStyleBackColor = true;
            this.StopSearchButton.Click += new System.EventHandler(this.StopSearchButton_Click);
            // 
            // ResumeSearchButton
            // 
            this.ResumeSearchButton.Location = new System.Drawing.Point(345, 122);
            this.ResumeSearchButton.Name = "ResumeSearchButton";
            this.ResumeSearchButton.Size = new System.Drawing.Size(180, 23);
            this.ResumeSearchButton.TabIndex = 10;
            this.ResumeSearchButton.Text = "Продолжить поиск";
            this.ResumeSearchButton.UseVisualStyleBackColor = true;
            this.ResumeSearchButton.Click += new System.EventHandler(this.ResumeSearchButton_Click);
            // 
            // CrashSearchButton
            // 
            this.CrashSearchButton.Location = new System.Drawing.Point(345, 151);
            this.CrashSearchButton.Name = "CrashSearchButton";
            this.CrashSearchButton.Size = new System.Drawing.Size(180, 23);
            this.CrashSearchButton.TabIndex = 11;
            this.CrashSearchButton.Text = "Полностью остановить поиск";
            this.CrashSearchButton.UseVisualStyleBackColor = true;
            this.CrashSearchButton.Click += new System.EventHandler(this.CrashSearchButton_Click);
            // 
            // ClearSearchButton
            // 
            this.ClearSearchButton.Location = new System.Drawing.Point(900, 561);
            this.ClearSearchButton.Name = "ClearSearchButton";
            this.ClearSearchButton.Size = new System.Drawing.Size(106, 23);
            this.ClearSearchButton.TabIndex = 12;
            this.ClearSearchButton.Text = "Отчистить поиск";
            this.ClearSearchButton.UseVisualStyleBackColor = true;
            this.ClearSearchButton.Click += new System.EventHandler(this.ClearSearchButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(314, 551);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Количество просмотренных файлов:";
            // 
            // FileCountLabel
            // 
            this.FileCountLabel.AutoSize = true;
            this.FileCountLabel.Location = new System.Drawing.Point(314, 566);
            this.FileCountLabel.Name = "FileCountLabel";
            this.FileCountLabel.Size = new System.Drawing.Size(34, 13);
            this.FileCountLabel.TabIndex = 14;
            this.FileCountLabel.Text = " -- -- --";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(314, 514);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Текущий файл:";
            // 
            // ThreadFileNameLabel
            // 
            this.ThreadFileNameLabel.AutoSize = true;
            this.ThreadFileNameLabel.Location = new System.Drawing.Point(314, 527);
            this.ThreadFileNameLabel.Name = "ThreadFileNameLabel";
            this.ThreadFileNameLabel.Size = new System.Drawing.Size(34, 13);
            this.ThreadFileNameLabel.TabIndex = 16;
            this.ThreadFileNameLabel.Text = " -- -- --";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1048, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Содержимое файла:";
            // 
            // SearchedListBox
            // 
            this.SearchedListBox.Enabled = false;
            this.SearchedListBox.FormattingEnabled = true;
            this.SearchedListBox.Location = new System.Drawing.Point(1051, 193);
            this.SearchedListBox.Margin = new System.Windows.Forms.Padding(0);
            this.SearchedListBox.Name = "SearchedListBox";
            this.SearchedListBox.Size = new System.Drawing.Size(404, 394);
            this.SearchedListBox.TabIndex = 18;
            // 
            // SearchedRichTextBox
            // 
            this.SearchedRichTextBox.AcceptsTab = true;
            this.SearchedRichTextBox.Location = new System.Drawing.Point(1051, 193);
            this.SearchedRichTextBox.Name = "SearchedRichTextBox";
            this.SearchedRichTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Horizontal;
            this.SearchedRichTextBox.Size = new System.Drawing.Size(404, 394);
            this.SearchedRichTextBox.TabIndex = 19;
            this.SearchedRichTextBox.Text = "";
            // 
            // FormForTestEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1467, 599);
            this.Controls.Add(this.SearchedRichTextBox);
            this.Controls.Add(this.SearchedListBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ThreadFileNameLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FileCountLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ClearSearchButton);
            this.Controls.Add(this.CrashSearchButton);
            this.Controls.Add(this.ResumeSearchButton);
            this.Controls.Add(this.StopSearchButton);
            this.Controls.Add(this.NewTreeViewForEx1);
            this.Controls.Add(this.FindNameTextBox);
            this.Controls.Add(this.FindTextTextBox);
            this.Controls.Add(this.SetFileButton);
            this.Controls.Add(this.SetDirWayButton);
            this.Controls.Add(this.TreeViewForInfo);
            this.Controls.Add(this.TreeViewForEx1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormForTestEx";
            this.Text = "SearchFile";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView TreeViewForEx1;
        private System.Windows.Forms.TreeView TreeViewForInfo;
        private System.Windows.Forms.OpenFileDialog OpenFileDialogForEx1;
        private System.Windows.Forms.Button SetDirWayButton;
        private System.Windows.Forms.FolderBrowserDialog FolderBrowserDialogForEx1;
        private System.Windows.Forms.Button SetFileButton;
        private System.Windows.Forms.TextBox FindTextTextBox;
        private System.Windows.Forms.TextBox FindNameTextBox;
        private System.Windows.Forms.TreeView NewTreeViewForEx1;
        private System.Windows.Forms.Button StopSearchButton;
        private System.Windows.Forms.Button ResumeSearchButton;
        private System.Windows.Forms.Button CrashSearchButton;
        private System.Windows.Forms.Button ClearSearchButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label FileCountLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label ThreadFileNameLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox SearchedListBox;
        private System.Windows.Forms.RichTextBox SearchedRichTextBox;
    }
}

