﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestEx
{
    public partial class FormForTestEx : Form
    {
        public FormForTestEx()
        {
            InitializeComponent();
        }

        private FunctionForSearch SETFUNC = new FunctionForSearch();

        private const int Current = 0;

        private const string DefaultFileText = "Файл содержит текст";
        private const string DefaultFileName = "Файл имеет имя";

        private bool CheckFileName = false;
        private bool CheckFileText = false;
        
        private string dirName;
        private string CurrentFileCatalog;
        private string CurrentFileName;
        private string CurrentFileText;

        public Thread thr;

        private int FileCount;
        
        private void FindAllRep(TreeNode Main, bool CheckCurrentRep, string[] files, List<string[]> ChildDirList, List<string[]> DirList, 
            List<TreeNode> Nodes, List<string[]> FilesList, TreeNode NewMain)
        {
            int Current = 0;
            
            while (DirList.Count != Current)
            {
                if (Directory.Exists(dirName))
                {
                    List<TreeNode> ChildMain = new List<TreeNode>();
                    TreeNode Child, LittleChild, C;

                    var FDL = DirList.First();

                    ChildMain.Add(Nodes.First());
                    var CM = ChildMain.First();

                    foreach (string FirstDir in FDL)
                    {
                        CM = Nodes.First().Nodes.Add(FirstDir);

                        FilesList.Add(Directory.GetFiles(FirstDir));
                        var FL = FilesList.First();


                        foreach (string CurrentString in FL)
                        {
                            Child = CM.Nodes.Add(CurrentString);
                            
                            AddFileName(CurrentString, NewMain);

                            FileCount++;
                            SETFUNC.CheckFileCount(CurrentString, FileCount, FileCountLabel, ThreadFileNameLabel);

                            ChildMain.Add(Child);
                        }

                        ChildDirList.Add(Directory.GetDirectories(FirstDir));

                        try
                        {
                            var CDR = ChildDirList.First();

                            foreach (string SecondDir in CDR)
                            {
                                LittleChild = CM.Nodes.Add(SecondDir);

                                files = Directory.GetFiles(SecondDir);

                                ChildDirList.Add(Directory.GetDirectories(SecondDir));

                                foreach (string CurrentString in files)
                                {
                                    C = LittleChild.Nodes.Add(CurrentString);

                                    AddFileName(CurrentString, NewMain);
                                    FileCount++;
                                    SETFUNC.CheckFileCount(CurrentString, FileCount, FileCountLabel, ThreadFileNameLabel);
                                }

                                ChildDirList.RemoveAt(Current);
                            }
                            ChildDirList.RemoveAt(Current);
                        }
                        catch (Exception Error)
                        {
                            MessageBox.Show(Error.Message);
                        }

                        FilesList.RemoveAt(Current);
                        DirList.Add(Directory.GetDirectories(FirstDir));
                    }
                    try
                    {
                        Nodes.Add(ChildMain.First());
                        Nodes.RemoveAt(Current);
                        ChildMain.RemoveAt(Current);
                    }
                    catch (Exception) { }

                    DirList.RemoveAt(Current);
                }
            }
        }

        private void AddFileName(string CurrentString, TreeNode NewMain) // Визуалка для отображение найденых файлов
        {
            if (CheckFileName == true)
            {
                if (CurrentString.Contains(CurrentFileName))
                {
                    var NS = CurrentString;
                    var CNS = NS.Length - 1;
                    string NewName = "";
                    var s = '\\';
                    while (NS.ElementAt(CNS) != s)
                    {
                        NewName = NS.ElementAt(CNS) + NewName;
                        CNS--;
                    }
                    NewMain.Nodes.Add(NewName).Tag = CurrentString;
                }
            }

            if (CheckFileText == true)
            {
                try
                {
                    var fileText = System.IO.File.ReadAllText(CurrentString);

                    if (fileText.Contains(CurrentFileText))
                    {
                        var NS = CurrentString;
                        var CNS = NS.Length - 1;
                        string NewName = "";
                        var s = '\\';
                        while (NS.ElementAt(CNS) != s)
                        {
                            NewName = NS.ElementAt(CNS) + NewName;
                            CNS--;
                        }
                        NewMain.Nodes.Add(NewName).Tag = CurrentString;
                    }
                }
                catch(Exception) {  }
            }
        }
        
        private void CreateTreeView()
        {
            BeginInvoke((MethodInvoker)delegate ()
            {
                TreeView NewNode = new TreeView();
                TreeNode Current, Main, NewMain;

                TreeViewForEx1.Nodes.Add("Main", "Поиск файлов.");
                NewTreeViewForEx1.Nodes.Add("Main", "Найденые файлы.");
                Main = TreeViewForEx1.Nodes.Add("Second", "Выбранный каталог: " + dirName);
                NewMain = NewTreeViewForEx1.Nodes.Add("Main", "Выбранный каталог: " + dirName);

                bool CheckCurrentRep = true;
                string[] dirs = Directory.GetDirectories(dirName); 
                string[] Newdirs = Directory.GetDirectories(dirName); 
                string[] files = Directory.GetFiles(dirName); 

                List<string[]> FilesList = new List<string[]>();

                List<string[]> ChildDirList = new List<string[]>();
                List<string[]> DirList = new List<string[]>();

                List<TreeNode> Nodes = new List<TreeNode>();

                foreach (string CurrentString in files)
                {
                    Current = Main.Nodes.Add(CurrentString);

                    AddFileName(CurrentString, NewMain);
                    FileCount++;

                    SETFUNC.CheckFileCount(CurrentString, FileCount, FileCountLabel, ThreadFileNameLabel);
                }

                Nodes.Add(Main);
                DirList.Add(dirs);
                FindAllRep(Main, CheckCurrentRep, files, ChildDirList, DirList, Nodes, FilesList, NewMain);
            });
        }
        
        private void SetDirWayButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialogForEx1.ShowDialog();

            if (FolderBrowserDialogForEx1.SelectedPath.ToString() != "")
            {
                try
                {
                    dirName = FolderBrowserDialogForEx1.SelectedPath.ToString();
                    FileCount = 0;

                    if (FindNameTextBox.Text != DefaultFileName)
                    {
                        CheckFileName = true;
                        CurrentFileName = FindNameTextBox.Text;
                    }

                    if (FindTextTextBox.Text != DefaultFileText)
                    {
                        CheckFileText = true;
                        CurrentFileText = FindTextTextBox.Text;
                    }

                    thr = new Thread((CreateTreeView));
                    thr.Name = "ForCreateTreeView";
                    thr.Start();
                    
                    DirectoryInfo dirInfo = new DirectoryInfo(dirName);
                    TreeViewForInfo.Nodes.Add("Название каталога: " + dirInfo.Name);
                    TreeViewForInfo.Nodes.Add("Полное название каталога: " + dirInfo.FullName);
                    TreeViewForInfo.Nodes.Add("Время создания каталога: " + dirInfo.CreationTime.ToShortDateString());
                    TreeViewForInfo.Nodes.Add("Корневой каталог: " + dirInfo.Root.ToString());
                }
                catch (Exception) { }
            }
        }

        private void SetFileButton_Click(object sender, EventArgs e)
        {
            string[] fileText;
            
            try
            {
                TreeViewForInfo.Nodes.Add("");
                TreeViewForInfo.Nodes.Add("Имя файла:");
                TreeViewForInfo.Nodes.Add(System.IO.Path.GetFileName(CurrentFileCatalog));

                fileText = System.IO.File.ReadAllLines(CurrentFileCatalog);
                                          
                foreach (string NewStr in fileText)
                {
                    SearchedRichTextBox.Text += "\n" + NewStr;
                }
            }
            catch(Exception Error) { String S = Error.Source; MessageBox.Show(Error.Message, S); }
        }

        private void NewTreeViewForEx1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                CurrentFileCatalog = NewTreeViewForEx1.SelectedNode.Tag.ToString();
            }
            catch (Exception Error) { MessageBox.Show(Error.Message); }
        }

        private void FindTextTextBox_Click(object sender, EventArgs e) => FindTextTextBox.Text = "";

        private void FindNameTextBox_Click(object sender, EventArgs e) => FindNameTextBox.Text = "";

        private void ClearSearchButton_Click(object sender, EventArgs e)
        {

        }

        /* Пауза/Остановка/Возобновление поиска */
        private void StopSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (thr.ThreadState == ThreadState.Running)
                {
                    thr.Suspend(); // Проверить не могу, просто не успеваю 
                }
            }
            catch(Exception) { thr.Abort(); }
        } 
        private void ResumeSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (thr.ThreadState == ThreadState.Stopped)
                {
                    thr.Resume(); // Аналогично
                }
            }
            catch(Exception) { thr.Abort(); }
        }
        private void CrashSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (thr.ThreadState == ThreadState.Stopped)
                {
                    thr.Abort(); // Аналогично
                }
            }
            catch(Exception) {  }
        }
        /* Пауза/Остановка/Возобновление поиска */
    }
}